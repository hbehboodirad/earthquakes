//
//  ViewStyles.swift
//  BehboodiWeather
//
//  Created by Hossein Behboudi Rad on 24/07/2020.
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

struct Styles {
    static func button(_ button: UIButton) {
        button.backgroundColor = .rbOrange
        button.setTitleColor(.rbBlack, for: .normal)
        cornerRadius(button)
    }
    
    static func container(_ view: UIView) {
        cornerRadius(view)
    }
}

extension Styles {
    static func cornerRadius(_ view: UIView) {
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
    }
}
