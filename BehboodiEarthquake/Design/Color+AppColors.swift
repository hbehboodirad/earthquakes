//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// An extension on UIColor to define some semantic colors for using in the app
///
/// This extension has been defined in order to define colors that is used in the app theme and make it
/// easy to change the theme of the app in future.
extension UIColor {
    
    open class var background: UIColor { #colorLiteral(red: 0.02745098039, green: 0.368627451, blue: 0.3294117647, alpha: 1) }
    open class var onBackgroundDefault: UIColor { return .white }
    open class var onBackgroundSupport: UIColor { #colorLiteral(red: 0.8667320609, green: 0.8615803123, blue: 0.8706926107, alpha: 1) }
    
    open class var foundation: UIColor { #colorLiteral(red: 0.9340894818, green: 0.9285365939, blue: 0.9383578897, alpha: 1) }
    
    open class var surface: UIColor { return .white }
    open class var onSurface: UIColor { return .black }
    
    open class var primary: UIColor { #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) }
    open class var onPrimary: UIColor { return .white }
    
    open class var accent: UIColor { #colorLiteral(red: 0.8774225712, green: 0.746560514, blue: 0, alpha: 1) }
    open class var onAccent: UIColor { return .black }
}
