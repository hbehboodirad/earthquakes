//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// Defines some stylings that is used for different elements in the app
///
/// This struct has been defined in order to define style that is used in the app theme and make it
/// easy to change the theme of the app in future.
struct Styles {
    
    // MARK: - Navigation Bar
       
    static func navigationBar(_ navigationBar: UINavigationBar) {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.onBackgroundDefault]
        navigationBar.largeTitleTextAttributes = textAttributes
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = UIColor.background
        navigationBar.backgroundColor = UIColor.background
        navigationBar.tintColor = UIColor.onBackgroundDefault
    }
    
    // MARK: - Views
    
    static func container(_ view: UIView) {
        cornerRadius(view)
    }
    
    // MARK: - Button
    
    static func button(_ button: UIButton) {
        button.backgroundColor = .primary
        button.setTitleColor(.onPrimary, for: .normal)
        cornerRadius(button)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
        button.contentEdgeInsets = .init(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    // MARK: - Labels
    
    static func titleLabel(_ label: UILabel) {
        label.font = UIFont.preferredFont(forTextStyle: .title1, weight: .bold)
    }
    
    static func bodyLabel(_ label: UILabel, important: Bool = false) {
        label.font = UIFont.preferredFont(forTextStyle: .body, weight: important ? .bold : .regular)
    }
    
    static func subTitleLabel(_ label: UILabel) {
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    }
    
    static func keyValueLabel(_ label: UILabel, key: String, value: String, size: UIFont.TextStyle = .subheadline) {
        //create key section
        let keyFormatted = "\(key): "
        let message = NSMutableAttributedString(string: keyFormatted,
                                                attributes: [.font: UIFont.preferredFont(forTextStyle: size, weight: .bold)])
        
        //create value section
        let value = NSAttributedString(string: value, attributes: [.font: UIFont.preferredFont(forTextStyle: size)])
        message.append(value)
        
        //return final message
        label.attributedText = message
    }
}

extension Styles {
    static func cornerRadius(_ view: UIView) {
        view.layer.cornerRadius = 12
        view.layer.masksToBounds = true
    }
}
