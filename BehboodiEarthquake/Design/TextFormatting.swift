//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation

/// Define different test formatting for text that accepts formats like date, amount or measures like meter, pressure and etc.
///
/// This formats make it easy to change the formatting in the whole app if we decided to change it in future.
struct TextFormatting {
    static func date(_ date: Date) -> String { date.toString(format: "d MMM yyyy HH:mm") }
}
