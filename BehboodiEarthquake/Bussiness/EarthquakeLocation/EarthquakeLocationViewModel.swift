//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation
import CoreLocation

/// Presents earthquake data or styles that earthquake location view requires
struct EarthquakeLocationViewModel {
    private let earthquake: Earthquake
    
    init(earthquake: Earthquake) {
        self.earthquake = earthquake
    }
    
    /// The location object that earthquake happend
    var location: CLLocationCoordinate2D { CLLocationCoordinate2D.init(latitude: earthquake.latitude, longitude: earthquake.longitude) }
}
