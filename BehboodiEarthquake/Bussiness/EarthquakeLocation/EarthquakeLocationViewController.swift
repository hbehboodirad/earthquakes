//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation
import MapKit

class EarthquakeLocationViewController: UIViewController {
    
    private var viewModel: EarthquakeLocationViewModel
    
    let mapView: MKMapView = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        Styles.cornerRadius($0)
        return $0
    }(MKMapView())
    
    init(viewModel: EarthquakeLocationViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        configureView()
        configureSubViews()
        guard let navigationBar = navigationController?.navigationBar else { return }
        Styles.navigationBar(navigationBar)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        mapView.setCenter(viewModel.location, animated: true)
    }
    
    func configureView() {
        title = localized("earthquakeLocation.title")
        navigationItem.largeTitleDisplayMode = .never
        view.backgroundColor = .foundation
    }
    
    func configureSubViews() {
        view.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: Spacer.lg),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Spacer.lg),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -1 * Spacer.lg),
            mapView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
        ])
    }
}
