//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// Builds an instance of earthquake location view
struct EarthquakeLocationBuilder {
    static func build(earthquake: Earthquake) -> UIViewController {
        let viewModel = EarthquakeLocationViewModel(earthquake: earthquake)
        let viewController = EarthquakeLocationViewController(viewModel: viewModel)
        return viewController
    }
}
