//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class EarthquakeTableViewCell: UITableViewCell {
    static let cellIdentifier = "DailyCellID"
//    
//    private let dayLabel: UILabel = {
//        Styles.bodyLabel($0)
//        $0.textColor = .onSurface
//        $0.numberOfLines = 0
//        $0.lineBreakMode = .byCharWrapping
//        return $0
//    }(UILabel())
//    private  let weatherIcon: UIImageView = {
//        $0.tintColor = .onSurface
//        $0.adjustsImageSizeForAccessibilityContentSizeCategory = true
//        return $0
//    }(UIImageView())
//    private let temperatureLabel: UILabel = {
//        Styles.bodyLabel($0)
//        $0.textColor = .onSurface
//        return $0
//    }(UILabel())
//    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        backgroundColor = .surface
//        configureSubViews()
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
    func configure(dailyWeather: EarthquakeViewModel) {
//        dayLabel.text = dailyWeather.dayName
//        temperatureLabel.text = dailyWeather.temperature
//        weatherIcon.image = dailyWeather.icon
    }
//    
//    // MARK: - Private
//    
//    func configureSubViews() {
//        [dayLabel, weatherIcon, temperatureLabel].forEach {
//            $0.translatesAutoresizingMaskIntoConstraints = false
//            addSubview($0)
//        }
//        
//        let iconCenterConstraint = weatherIcon.centerXAnchor.constraint(equalTo: centerXAnchor)
//        iconCenterConstraint.priority = .defaultHigh
//        NSLayoutConstraint.activate([
//            dayLabel.topAnchor.constraint(equalTo: topAnchor, constant: Spacer.md),
//            dayLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Spacer.lg),
//            dayLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
//            dayLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1 * Spacer.md),
//            
//            weatherIcon.leadingAnchor.constraint(greaterThanOrEqualTo: dayLabel.trailingAnchor, constant: Spacer.md),
//            weatherIcon.widthAnchor.constraint(equalTo: weatherIcon.heightAnchor, multiplier: 1),
//            weatherIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
//            iconCenterConstraint,
//            
//            temperatureLabel.leadingAnchor.constraint(greaterThanOrEqualTo: weatherIcon.trailingAnchor, constant: Spacer.md),
//            temperatureLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1 * Spacer.lg),
//            temperatureLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
//        ])
//    }
}
