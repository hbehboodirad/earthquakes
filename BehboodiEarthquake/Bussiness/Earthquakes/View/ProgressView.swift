//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class LoadingView: UIView {
       
    let messageLabel: UILabel = {
        Styles.titleLabel($0)
        $0.textColor = .rbWhite
        $0.textAlignment = .center
        return $0
    }(UILabel())
    let activityIndicator: UIActivityIndicatorView = {
        $0.color = .rbWhite
        return $0
    }(UIActivityIndicatorView())
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        //styling
        backgroundColor = .rbBlue
        
        //subviews
        configureSubViews()
    }
    
    // MARK: - Private
    
    private func configureSubViews() {
        [messageLabel, activityIndicator].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
        
        let margin: CGFloat = 8
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: margin),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: margin),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1 * margin),
            messageLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 3 * margin),
            
            activityIndicator.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 2 * margin),
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1 * margin)
        ])
    }
}
