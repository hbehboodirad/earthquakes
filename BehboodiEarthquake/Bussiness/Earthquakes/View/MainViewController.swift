//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class EarthquakesViewController: UIViewController {
    
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var viewFailure: ErrorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var todayWeatherContainerView: DayWeatherView!
    @IBOutlet weak var dailyWeatherTableView: UITableView!
    @IBOutlet weak var refreshButton: UIButton!
    
    var presenter: EarthquakesPresentering?
    
    private var dailyData: [DailyWeatherViewModel] = []
    
    private var dataLoader: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubViews()
        showProgressView()
        self.dataLoader = presenter?.bind(onLoadingStarted: showProgressView,
                                          onDataLoaded: showData,
                                          onLoadingFailed: showErrorMessage)
        dataLoader?()
    }
}

// MARK: - Selector
extension EarthquakesViewController {
    @objc func didTapErrorButton() {
        dataLoader?()
    }
    @IBAction func didTapRefreshButton(_ sender: Any) {
        dataLoader?()
    }
}

// MARK: - TableView
extension EarthquakesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dailyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let queuedCell = tableView.dequeueReusableCell(withIdentifier: DailyTableViewCell.cellIdentifier, for: indexPath)
        guard let cell = queuedCell as? DailyTableViewCell else { return UITableViewCell() }
        
        cell.configure(dailyWeather: dailyData[indexPath.row])
        return cell
    }
}

// MARK: - Private
extension EarthquakesViewController {
    private func configureSubViews()  {
        view.backgroundColor = .background
        
        //today view
        Styles.bodyLabel(todayLabel)
        todayLabel.textColor = .onBackgroundDefault
        
        //progress view
        loadingView.messageLabel.text = localized("mainPage.labels.loading")

        //daily view
        Styles.container(dailyWeatherTableView)
        dailyWeatherTableView.register(DailyTableViewCell.self, forCellReuseIdentifier: DailyTableViewCell.cellIdentifier)
        dailyWeatherTableView.rowHeight = UITableView.automaticDimension
        dailyWeatherTableView.estimatedRowHeight = 35

        //error view
        viewFailure.actionButton.setTitle(localized("mainPage.buttons.retry"), for: .normal)
        viewFailure.actionButton.addTarget(self, action: #selector(didTapErrorButton), for: .touchUpInside)
        
        //refresh button
        Styles.button(refreshButton)
        refreshButton.setTitle(localized("mainPage.buttons.refresh"), for: .normal)
    }
    
    private func showProgressView() {
        scrollView.isHidden = true
        viewFailure.isHidden = true
        loadingView.isHidden = false
    }
    
    private func hideProgressView() {
        loadingView.isHidden = true
    }
    
    private func showData(weather: WeatherViewModel) {
        hideProgressView()
        
        //update info view
        todayWeatherContainerView.configure(weather: weather)
        Styles.keyValueLabel(todayLabel, key: localized("mainPage.labels.today"), value: weather.current.dateString, size: .body)
        
        //update daily data
        //        dailyData = weather.days
        //        dailyWeatherTableView.reloadData()//TODO: take care of empty daily
        //        dailyWeatherTableView.layoutIfNeeded()
        dailyWeatherTableView.heightAnchor.constraint(equalToConstant: dailyWeatherTableView.contentSize.height).isActive = true
        
        //config view visibility
        scrollView.isHidden = false
    }
    
    private func showErrorMessage(message: String) {
        viewFailure.updatedMessage(message: message)
        
        viewFailure.isHidden = false
        scrollView.isHidden = true
        loadingView.isHidden = true
    }
}
