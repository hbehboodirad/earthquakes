//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class EarthquakesViewController: UIViewController {
    
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var viewFailure: ErrorView!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: EarthquakesPresentering?
    var router: EarthquakesRouting?
    
    private var earthquakes: [EarthquakeViewModel] = []
    private var onDataRequired: (() -> Void)?
    private var onItemSelected: ((EarthquakeViewModel, UINavigationController) -> Void)?
    
    override func loadView() {
        super.loadView()
        configureNavigationBar()
        configureSubViews()
        showProgressView()
        setupBinding()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onDataRequired?()
    }
}

// MARK: - Selector
extension EarthquakesViewController {
    @objc func didTapErrorButton() {
        onDataRequired?()
    }
}

// MARK: - TableView
extension EarthquakesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return earthquakes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let queuedCell = tableView.dequeueReusableCell(withIdentifier: EarthquakeTableViewCell.cellIdentifier, for: indexPath)
        guard let cell = queuedCell as? EarthquakeTableViewCell else { return UITableViewCell() }
        
        cell.configure(earthquake: earthquakes[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let navigationController = self.navigationController else { return }
        onItemSelected?(earthquakes[indexPath.row], navigationController)
    }
}

// MARK: - Private
extension EarthquakesViewController {
    
    private func configureNavigationBar() {
        self.navigationItem.largeTitleDisplayMode = .always
        self.title = localized("mainPage.title")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        guard let navigationBar = navigationController?.navigationBar else { return }
        navigationBar.prefersLargeTitles = true
    }
    
    private func configureSubViews()  {
        view.backgroundColor = .background
        
        //progress view
        loadingView.messageLabel.text = localized("mainPage.labels.loading")

        //daily view
        Styles.container(tableView)
        tableView.register(EarthquakeTableViewCell.self, forCellReuseIdentifier: EarthquakeTableViewCell.cellIdentifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 35
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self

        //error view
        viewFailure.actionButton.setTitle(localized("mainPage.buttons.retry"), for: .normal)
        viewFailure.actionButton.addTarget(self, action: #selector(didTapErrorButton), for: .touchUpInside)
    }
    
    private func setupBinding() {
        let output = presenter?.bind(
            onLoadingStarted: showProgressView,
            onDataLoaded: showData,
            onLoadingFailed: showErrorMessage
        )
        onDataRequired = output?.onDataRequired
        onItemSelected = output?.onItemSelected
    }
    
    private func showProgressView() {
        tableView.isHidden = true
        viewFailure.isHidden = true
        loadingView.isHidden = false
    }
    
    private func hideProgressView() {
        loadingView.isHidden = true
    }
    
    private func showData(earthquakes: [EarthquakeViewModel]) {
        hideProgressView()
                
        //update daily data
        self.earthquakes = earthquakes
        tableView.reloadData()//TODO: take care of empty daily
        
        //config view visibility
        tableView.isHidden = false
    }
    
    private func showErrorMessage(message: String) {
        viewFailure.updatedMessage(message: message)
        
        viewFailure.isHidden = false
        tableView.isHidden = true
        loadingView.isHidden = true
    }
}
