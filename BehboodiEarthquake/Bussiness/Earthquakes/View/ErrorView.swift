//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class ErrorView: UIView {
    
    let messageLabel: UILabel = {
        Styles.bodyLabel($0)
        $0.textColor = .onSurface
        $0.textAlignment = .center
        $0.numberOfLines = 0
        $0.lineBreakMode = .byWordWrapping
        return $0
    }(UILabel())
    let actionButton: UIButton = {
        Styles.button($0)
        return $0
    }(UIButton())
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        //styling
        Styles.container(self)
        backgroundColor = .surface
        
        //subviews
        configureSubViews()
    }
    
    func updatedMessage(message: String) {
        messageLabel.text = message
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    // MARK: - Private
    
    private func configureSubViews() {
        [messageLabel, actionButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
        
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: Spacer.md),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Spacer.md),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1 * Spacer.md),
            messageLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: Spacer.xlg),
            
            actionButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Spacer.lg),
            actionButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            actionButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1 * Spacer.md),
            actionButton.widthAnchor.constraint(greaterThanOrEqualToConstant: 80)
        ])
    }
}
