//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class EarthquakeTableViewCell: UITableViewCell {
    static let cellIdentifier = "EarthquakeCellID"
   
    private let containerView: UIView = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        Styles.container($0)
        $0.backgroundColor = .surface
        return $0
    }(UIView())
    private let magnitudeLabel: UILabel = {
        Styles.bodyLabel($0, important: true)
        $0.textColor = .onSurface
        return $0
    }(UILabel())
    private let dateLabel: UILabel = {
        Styles.subTitleLabel($0)
        $0.textColor = .onSurface
        return $0
    }(UILabel())
    private let latitudeLabel: UILabel = {
        Styles.bodyLabel($0)
        $0.textColor = .onSurface
        return $0
    }(UILabel())
    private let longitudeLabel: UILabel = {
        Styles.bodyLabel($0)
        $0.textColor = .onSurface
        return $0
    }(UILabel())
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .surface
        selectionStyle = .none
        configureSubViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(earthquake: EarthquakeViewModel) {
        magnitudeLabel.text = earthquake.magnitude
        dateLabel.text = earthquake.date
        Styles.keyValueLabel(latitudeLabel, key: localized("mainPage.labels.latitude"), value: earthquake.latitude)
        Styles.keyValueLabel(longitudeLabel, key: localized("mainPage.labels.longitude"), value: earthquake.longitude)
        containerView.backgroundColor = earthquake.backgroundColor
        [dateLabel, magnitudeLabel, latitudeLabel, longitudeLabel].forEach { $0.textColor = earthquake.textColor }
    }
    
    // MARK: - Private
    
    func configureSubViews() {
        backgroundColor = .clear
        addSubview(containerView)
        [dateLabel, magnitudeLabel, latitudeLabel, longitudeLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            containerView.addSubview($0)
        }
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: Spacer.sm),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Spacer.md),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1 * Spacer.md),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1 * Spacer.sm),

            magnitudeLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: Spacer.md),
            magnitudeLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: Spacer.md),
            magnitudeLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 1),

            dateLabel.topAnchor.constraint(equalTo: magnitudeLabel.topAnchor),
            dateLabel.leadingAnchor.constraint(greaterThanOrEqualTo: magnitudeLabel.trailingAnchor, constant: Spacer.md),
            dateLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -1 * Spacer.md),
            dateLabel.bottomAnchor.constraint(greaterThanOrEqualTo: magnitudeLabel.bottomAnchor),
            dateLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 1),
            
            latitudeLabel.topAnchor.constraint(equalTo: magnitudeLabel.bottomAnchor, constant: Spacer.md),
            latitudeLabel.leadingAnchor.constraint(equalTo: magnitudeLabel.leadingAnchor),
            latitudeLabel.trailingAnchor.constraint(equalTo: dateLabel.trailingAnchor),
            
            longitudeLabel.topAnchor.constraint(equalTo: latitudeLabel.bottomAnchor, constant: Spacer.md),
            longitudeLabel.leadingAnchor.constraint(equalTo: magnitudeLabel.leadingAnchor),
            longitudeLabel.trailingAnchor.constraint(equalTo: dateLabel.trailingAnchor),
            longitudeLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -1 * Spacer.md)
        ])
        
        magnitudeLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        dateLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    }
}
