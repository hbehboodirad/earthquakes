//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation

class EarthquakesPresenter: NSObject {
    var interactor: EarthquakesInteracting?
    var wireframe: EarthquakesRouting?
    var criticalAreaQueue = DispatchQueue(label: "CriticalAreaForCheckingLoadOfAllData")
    
    internal let targetCity: String = "London"
    
    private var currentData: CurrentWeatherViewModel?
    
    private var onLoadingStarted: (() -> Void)?
    private var onDataLoaded: ((WeatherViewModel) -> Void)?
    private var onLoadingFailed: ((String) -> Void)?
}

// MARK: - Weather Presenter
extension EarthquakesPresenter: WeatherPresenterProtocol {
    
    func bind(onLoadingStarted: @escaping () -> Void,
              onDataLoaded: @escaping (WeatherViewModel) -> Void,
              onLoadingFailed: @escaping (String) -> Void) -> () -> Void {
        self.onLoadingStarted = onLoadingStarted
        self.onDataLoaded = onDataLoaded
        self.onLoadingFailed = onLoadingFailed
        
        return loadData
    }
}

// MARK: - Private
extension EarthquakesPresenter {

    private func loadData() {
        // make cached data for detecting load of all data.
        // this method is not calling from multi-threads so do not required synchronizing
        currentData = nil
        
        onLoadingStarted?()
        
        //ask interactor to load all data
        interactor?.retrieveDataFromServer(cityName: targetCity) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                self.didRetrievedCurrentWhetherSuccessfully(data: data)
            case .failure(let error):
                self.onLoadingFailed?(error.localizedDescription)
            }
        }
    }
    
    private func didRetrievedCurrentWhetherSuccessfully(data: MomentWeather) {
        //create current weather data
        let viewModel = CurrentWeatherViewModel(weather: data)
        
        //synchronize setting current data to avoid race condition
        criticalAreaQueue.sync { currentData = viewModel }
        
        updateViewIfAllDataAvailable()
    }
    
    private func updateViewIfAllDataAvailable() {
        // make reading states synchronize to avoid race condition
        criticalAreaQueue.sync {[weak self] in
            //if all data has been loaded send data to view
            if let current = currentData {
                let viewModel = WeatherViewModel(current: current)
                self?.onDataLoaded?(viewModel)
            }
        }
    }
}
