//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation
import UIKit

/// Define functionalities that a presenter must provide for a view of earthquakes screen
///
/// Presenter is responsible for providing data from datastore, collect them  and convert them into
/// a format that view needs and deliver them properly to view
protocol EarthquakesPresentering {
    /// Instance of a interactor that can provide earthquakes
    var interactor: EarthquakesInteracting? { get set }
    
    /// Instance of the routings that the Earthquakes screen have
    var router: EarthquakesRouting? { get set }
    
    /// Binds a view to this presenter
    ///
    /// The binding means, the presenter get some methods that needs to be called at some meaningful moments
    /// and returns a list of methods that view can call on some meaningful moment to get achieve some goals
    ///
    /// - Parameters:
    ///     - onLoadingStarted: a method that presenter should call when it start loading new data
    ///     - onDataLoaded: a method that presenter should call when loading of data completed successfully
    ///     - onLoadingFailed: a method that presenter should call when loading data failed
    /// - Returns:
    ///     - onDataRequired: a method that view can when it requires a new and fresh data
    ///     - onItemSelected: a method that view can call when use selects an item and want to see it's detail
    func bind(
        onLoadingStarted: @escaping () -> Void,
        onDataLoaded: @escaping ([EarthquakeViewModel]) -> Void,
        onLoadingFailed: @escaping (String) -> Void
    ) -> (
        onDataRequired: () -> Void,
        onItemSelected: (EarthquakeViewModel, UINavigationController) -> Void
    )
}

class EarthquakesPresenter: EarthquakesPresentering {
    // dependencies
    var interactor: EarthquakesInteracting?
    var router: EarthquakesRouting?
    
    // Event handlers
    private var onLoadingStarted: (() -> Void)?
    private var onDataLoaded: (([EarthquakeViewModel]) -> Void)?
    private var onLoadingFailed: ((String) -> Void)?
    
    // In a real app following values comes from user interactions
    private let north = 44.1
    private let south = -9.9
    private let east = -22.4
    private let west = 55.2
    
    // MARK: - Weather Presenter
    
    func bind(
        onLoadingStarted: @escaping () -> Void,
        onDataLoaded: @escaping ([EarthquakeViewModel]) -> Void,
        onLoadingFailed: @escaping (String) -> Void
    ) -> (
        onDataRequired: () -> Void,
        onItemSelected: (EarthquakeViewModel, UINavigationController) -> Void
        ) {
            self.onLoadingStarted = onLoadingStarted
            self.onDataLoaded = onDataLoaded
            self.onLoadingFailed = onLoadingFailed
            
            return (loadData, onItemSelected)
    }
    
    // MARK: - Private
    
    private func loadData() {
        onLoadingStarted?()
        
        //ask interactor to load all data
        interactor?.retrieveDataFromServer(north: north, south: south, east: east, west: west) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let earthquakes):
                let earthquakes = earthquakes.earthquakes.map { EarthquakeViewModel(earthquake: $0) }
                self.onDataLoaded?(earthquakes)
            case .failure(let error):
                self.onLoadingFailed?(error.localizedDescription)
            }
        }
    }
    
    private func onItemSelected(earthquakeViewModel: EarthquakeViewModel, navigationController: UINavigationController) {
        guard let router = router else { return }
        router.didEarthquakeTapped(earthquake: earthquakeViewModel.earthquake, navigationController: navigationController)
    }
}
