//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// Builds an instance of earthquakes view
struct EarthquakesBuilder {
    static let mainViewControllerId = "MainViewControllerID"
    static var mainStoryboard: UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    static func build() -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: mainViewControllerId)
        
        if let view = viewController as? EarthquakesViewController {
            let presenter = EarthquakesPresenter()
            let interactor  = EarthquakesInteractor()
            let router = EarthquakesRouter()
            
            view.presenter = presenter
            presenter.router = router
            presenter.interactor = interactor
                        
            return viewController
        }
        return UIViewController()
    }
}
