//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// Define routes that the earthquakes page needs
///
/// A router abstracts and decouple concerns of navigation in the app from how view implemented so that
/// you can keep a view and easily just change it's routes by injecting new routers
protocol EarthquakesRouting {
    /// Do the required routing when an earthquake has been tapped
    ///
    /// - Parameters:
    ///     - earthquake:the selected earthquake
    ///     - navigationController: instance of a navigation controller required to do this routing
    func didEarthquakeTapped(earthquake: Earthquake, navigationController: UINavigationController)
}

struct EarthquakesRouter: EarthquakesRouting {
    func didEarthquakeTapped(earthquake: Earthquake, navigationController: UINavigationController) {
        let viewController = EarthquakeLocationBuilder.build(earthquake: earthquake)
        navigationController.pushViewController(viewController, animated: true)
    }
}
