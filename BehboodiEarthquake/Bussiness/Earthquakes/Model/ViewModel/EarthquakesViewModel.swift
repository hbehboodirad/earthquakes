//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// Presents earthquake data or styles that earthquake view requires
struct EarthquakeViewModel {
    
    let earthquake: Earthquake
    
    init(earthquake: Earthquake) {
        self.earthquake = earthquake
    }
    
    /// Date of the earthquake
    var date: String {
        guard let date = earthquake.dateTime.toDate(format: "yyyy-MM-dd HH:mm:ss") else { return "" }
        return TextFormatting.date(date)
    }
    
    /// Latitude of the earthquake
    var latitude: String { "\(earthquake.latitude)" }
    
    /// Longitude of the earthquake
    var longitude: String { "\(earthquake.longitude)" }
    
    /// Magnitude of the earthquake
    var magnitude: String { "\(earthquake.magnitude)" }
    
    /// Background color of the earthquake view
    var backgroundColor: UIColor { earthquake.magnitude >= 8.0 ? .accent : .surface }
    
    /// Text colors of the earthquake view
    var textColor: UIColor { earthquake.magnitude >= 8.0 ? .onAccent : .onSurface }
}
