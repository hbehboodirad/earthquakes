//
//  Earthquakes.swift
//  BehboodiEarthquake
//
//  Created by Hossein Behboudi Rad on 17/08/2020.
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation

/// DTO represents Earthquakes list
struct Earthquakes: Decodable {
    let earthquakes: [Earthquake]
}

/// DTO represents data of a earthquake
struct Earthquake: Decodable, Equatable {
    
    /// date time that the earthquake happened
    let dateTime: String
    
    /// Longitude of the location that earthquake happened
    let longitude: Double
    
    /// Latitude of the location that earthquake happened
    let latitude: Double
    
    /// Magnitude of the earthquake
    let magnitude: Double
    
    /// describes the keys that server sends for each item
    enum CodingKeys: String, CodingKey {
        case dateTime = "datetime"
        case longitude = "lng"
        case latitude = "lat"
        case magnitude
    }
}
