//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/// Protocol that define functionalities of an interactor that loads the earthquakes data
///
/// Interactors are responsible to load data from a data storage. it can be database, cache, network or etc.
/// Generally their purpose is to abstract data loading from other aspects of the app.
///
/// From communication perspective generally it is what presenter calls from interactor.
protocol EarthquakesInteracting {

    /// Retrieve earthquakes data from server
    ///
    /// - Parameters:
    ///     - north: north data that is required by API
    ///     - south: south data that is required by API
    ///     - east: east data that is required by API
    ///     - west: west data that is required by API
    ///     - completion: a closure that will be called when retrieve operation done. It will pass
    ///                   a result object with a Earthquakes for success case and Error for failure case
    func retrieveDataFromServer(north: Double,
                                south: Double,
                                east: Double,
                                west: Double,
                                completion: @escaping ((Result<Earthquakes, Error>) -> Void))
}

struct EarthquakesInteractor: EarthquakesInteracting {
    
    var webServiceLauncher: WebServiceLauncherProtocol = WebServiceLauncher()
            
    // MARK: - EarthquakesInteracting
    func retrieveDataFromServer(north: Double,
                                south: Double,
                                east: Double,
                                west: Double,
                                completion: @escaping ((Result<Earthquakes, Error>) -> Void)) {
        let request = EarthquakesWebRequest(baseURL: EarthquakesRequestURL.earthquakes.rawValue,
                                        north: north,
                                        south: south,
                                        east: east,
                                        west: west)
        let webService = WebServiceCreator.makeWebService(webRequest: request)
        
        webServiceLauncher.webService = webService
        
        webServiceLauncher.callWebService { result in
            switch result {
            case .success(let data):
                self.parse(data, with: completion)
            case .failure(let error):
                self.pass(error, to: completion)
            }
        }
    }
    
    // MARK: - Private
    private func parse(_ jsonData: Data, with completion: @escaping ((Result<Earthquakes, Error>) -> Void)) {
        //do data conversion and deserialization on back thread
        DispatchQueue.global().async {
            guard let response = try? JSONDecoder().decode(Earthquakes.self, from: jsonData) else {
                self.pass(InteractorError.parsingError, to: completion)
                return
            }
            DispatchQueue.main.async { completion(.success(response)) }
        }
    }
    
    private func pass(_ error: Error, to completion: @escaping ((Result<Earthquakes, Error>) -> Void)) {
        DispatchQueue.main.async { completion(.failure(error)) }
    }
}
