//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

class EarthquakesInteractor: EarthquakesInteracting {    
    var currentWhetherWebServiceLauncher: WebServiceLauncherProtocol = WebServiceLauncher()
    
    var deserializationQueue = DispatchQueue(label: "Deserialization")
    var mainQueue = DispatchQueue.main
        
    func retrieveDataFromServer(cityName: String, completion: @escaping ((Result<MomentWeather, Error>) -> Void)) {
        let request = WeatherWebRequest(baseURL: WeatherRequestURL.currentWeather.rawValue, cityName: cityName)
        let webService = WebServiceCreator.makeWebService(webRequest: request)
                
        currentWhetherWebServiceLauncher.webService = webService
        
        currentWhetherWebServiceLauncher.callWebService { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let data):
                self.parseAndDeliverCurrentWeather(jsonData: data, completion: completion)
            case .failure(let error):
                self.passError(error: error, completion: completion)
            }
        }
    }
    
    // MARK: - Private
    
    /// - Important: The method is calling from back thread
    private func parseAndDeliverCurrentWeather(jsonData: Data, completion: @escaping ((Result<MomentWeather, Error>) -> Void)) {
        //do data conversion and deserialization on back thread
        deserializationQueue.async { [weak self] in
            guard let response = try? JSONDecoder().decode(MomentWeather.self, from: jsonData) else {
                self?.passError(error: InteractorError.parsingError, completion: completion)
                return
            }
            self?.mainQueue.async { completion(.success(response)) }
        }
    }
    
    private func passError(error: Error, completion: @escaping ((Result<MomentWeather, Error>) -> Void)) {
        mainQueue.async { completion(.failure(error)) }
    }
}
