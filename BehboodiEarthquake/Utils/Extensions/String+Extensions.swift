//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/**
 Provides some helper methods for String
 */
extension String {
    /// Converts the string to date
    ///
    /// - Parameters:
    ///     - format: date format of the string presentation
    /// - Returns: a Date instance if it was successful to create based on the given format and nil if it fails
    func toDate(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}
