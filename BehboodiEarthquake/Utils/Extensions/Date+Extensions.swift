//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit

/**
 Provides some helper methods for converting string and date into each other.
 */
extension Date {
    /**
     Converts the Date to String with specific format
     
     - Parameters:
        - format: the format that is expected for string representing of the date
     
     - Returns: a String that represents the Date object with specific format. Returns empty string if it fails
     */
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        
        return dateFormatter.string(from: self)
    }
}
