//
//  Copyright © 2020 HBR. All rights reserved.
//

/// Enum of HTTP Methods 
enum WebClientHTTPMethod: String {
    ///HTTP POST method
    case POST
    
    ///HTTP GET method
    case GET
}
