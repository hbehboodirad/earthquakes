//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation

/// Provides URL for all endpoint of web-services.
enum EarthquakesRequestURL: String {
    ///URL for fetching earthquakes list
    case earthquakes = "http://api.geonames.org/earthquakesJSON"
}
