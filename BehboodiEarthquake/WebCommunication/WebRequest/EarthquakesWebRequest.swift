//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation

/// Request protocol that web service uses
///
/// This protocol provides what web service needs to get from the request to create its data and send it to the server.
protocol WebRequestProtocol {
    /// URL that web services use for sending the request to
    var url: String { get }
}

/// Request instance for the Earthquake web endpoints
struct EarthquakesWebRequest: WebRequestProtocol {
    var url: String {
        return "\(baseURL)" +
        "?formatted=true&north=\(self.north)&south=\(self.south)&east=\(self.east)&west=\(self.west)&username=\(self.username)"
    }
    
    /// Base part of the URL
    ///
    /// Base part includes host and path to the end point
    let baseURL: String
    
    //TODO: Add proper documentation for following fields required by API
    let north: Double
    let south: Double
    let east: Double
    let west: Double
    
    /// Username required by the API
    let username: String = "mkoppelman" //generally should be filled by an authentication layer
}
