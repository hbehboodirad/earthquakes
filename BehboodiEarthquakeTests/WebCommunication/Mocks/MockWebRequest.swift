//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation
@testable import BehboodiEarthquake

struct MockWebRequest: WebRequestProtocol {
    var mockUrl: String
    var url: String { return mockUrl }
}
