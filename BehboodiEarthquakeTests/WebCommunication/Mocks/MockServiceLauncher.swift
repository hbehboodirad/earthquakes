//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation
@testable import BehboodiEarthquake

class MockServiceLauncher: WebServiceLauncherProtocol {
    
    // MARK: - Mock implementation
    
    var webService: WebService?
    
    func callWebService(completion: WebCommunicationCompletionHandler?) {
        callWebServiceCalled = true
        guard let result = mockResult else { return }
        completion?(result)
    }
    
    // MARK: - Data for test verification
    
    var callWebServiceCalled = false
    var mockResult: Result<Data, Error>?
}
