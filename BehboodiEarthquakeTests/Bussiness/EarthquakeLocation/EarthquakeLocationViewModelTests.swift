//
//  EarthquakeLocationViewModelTests.swift
//  BehboodiEarthquakeTests
//
//  Created by Hossein Behboudi Rad on 19/08/2020.
//  Copyright © 2020 HBR. All rights reserved.
//

import XCTest
@testable import BehboodiEarthquake

class EarthquakeLocationViewModelTests: XCTestCase {

    func testExample() {
        // arrange
        let testEarthquake = Earthquake(dateTime: "2020-01-01 01:01:01", longitude: 1.2, latitude: 3.4, magnitude: 5)
        let viewModel = EarthquakeLocationViewModel(earthquake: testEarthquake)
        
        // act
        let location = viewModel.location
        
        // assert
        XCTAssertEqual(location.latitude, testEarthquake.latitude)
        XCTAssertEqual(location.longitude, testEarthquake.longitude)
    }
}
