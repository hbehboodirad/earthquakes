//
//  Copyright © 2020 HBR. All rights reserved.
//

import Foundation
@testable import BehboodiEarthquake

class MockInteractor: EarthquakesInteracting {
    
    func retrieveDataFromServer(north: Double,
                                south: Double,
                                east: Double,
                                west: Double,
                                completion: @escaping ((Result<Earthquakes, Error>) -> Void)) {
        retrieveMethodCalled = true
        receivedNorth = north
        receivedSouth = south
        receivedEast = east
        receivedWest = west
        guard let result = mockResult else { return }
        completion(result)
    }
    
    var retrieveMethodCalled: Bool = false
    var receivedNorth: Double?
    var receivedSouth: Double?
    var receivedEast: Double?
    var receivedWest: Double?
    var mockResult: Result<Earthquakes, Error>?
}
