//
//  Copyright © 2020 HBR. All rights reserved.
//

import UIKit
@testable import BehboodiEarthquake

class MockRouter: EarthquakesRouting {
    
    // MARK: - Mock Implementation

    func didEarthquakeTapped(earthquake: Earthquake, navigationController: UINavigationController) {
        routerCalled = true
        receivedEarthquake = earthquake
        receivedNavigationController = navigationController
    }
    
    // MARK: - Data for test verification
    
    var routerCalled = false
    var receivedEarthquake: Earthquake?
    var receivedNavigationController: UINavigationController?
}
