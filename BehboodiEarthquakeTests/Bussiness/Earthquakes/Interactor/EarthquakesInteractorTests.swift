//
//  Copyright © 2020 HBR. All rights reserved.
//

import XCTest
@testable import BehboodiEarthquake

class EarthquakesInteractorTests: XCTestCase {
    var interactor: EarthquakesInteractor!

    var mockServiceLauncher: MockServiceLauncher!

    var mockDeserializeQueue: DispatchQueue!
    var mockNorth = 1.2
    var mockSouth = 3.4
    var mockEast = 4.5
    var mockWest = 5.6

    override func setUp() {
        interactor = EarthquakesInteractor()

        mockServiceLauncher = MockServiceLauncher()
        interactor.webServiceLauncher = mockServiceLauncher
    }
    
    func testRetrive() {
        // act
        interactor.retrieveDataFromServer(north: mockNorth,
                                          south: mockSouth,
                                          east: mockEast,
                                          west: mockWest) { _ in }

        // assert ...

        //1- check service launcher called
        XCTAssert(mockServiceLauncher.callWebServiceCalled)

        //3- requested data is correct
        if let request = mockServiceLauncher.webService?.webRequest as? EarthquakesWebRequest {
            XCTAssertEqual(request.baseURL, EarthquakesRequestURL.earthquakes.rawValue)
            XCTAssertEqual(request.north, mockNorth)
            XCTAssertEqual(request.south, mockSouth)
            XCTAssertEqual(request.east, mockEast)
            XCTAssertEqual(request.west, mockWest)
        } else {
            XCTFail("Request type is not correct")
        }
    }
    
    func testRetriveSucceed() {
        let sampleJson = """
                                {"earthquakes": [{
                                  "datetime": "2011-03-11 04:46:23",
                                  "depth": 24.4,
                                  "lng": 142.369,
                                  "src": "us",
                                  "eqid": "c0001xgp",
                                  "magnitude": 8.8,
                                  "lat": 38.322
                                }]
                                }
                                """
        mockServiceLauncher.mockResult = Result<Data, Error>.success(Data(sampleJson.utf8))
        
        // act
        var receivedResult: Result<Earthquakes, Error>?
        let callingCompletionExpectation = expectation(description: "callingCompletion")
        interactor.retrieveDataFromServer(north: mockNorth,
                                          south: mockSouth,
                                          east: mockEast,
                                          west: mockWest) {
                                            receivedResult = $0
                                            callingCompletionExpectation.fulfill()
        }

        // wait for expectation
        wait(for: [callingCompletionExpectation], timeout: 0.1)
        
        // assert ...

        //check data received
        XCTAssertNotNil(receivedResult)

        //check correctness of data
        switch receivedResult {
        case .success(let earthquakes):
            guard earthquakes.earthquakes.count == 1 else {
                XCTFail("data not delivered as expected")
                return
            }
            let earthquake = earthquakes.earthquakes[0]
            XCTAssertEqual(earthquake.dateTime, "2011-03-11 04:46:23")
            XCTAssertEqual(earthquake.latitude, 38.322)
            XCTAssertEqual(earthquake.longitude, 142.369)
            XCTAssertEqual(earthquake.magnitude, 8.8)
        default:
            XCTFail("data not delivered correctly")
        }
    }

    func testRetrieveFailed() {
        mockServiceLauncher.mockResult = Result<Data, Error>.failure(MockError.mockError)
        
        // act
        var receivedResult: Result<Earthquakes, Error>?
        let callingCompletionExpectation = expectation(description: "callingCompletion")
        interactor.retrieveDataFromServer(north: mockNorth,
                                          south: mockSouth,
                                          east: mockEast,
                                          west: mockWest) {
                                            receivedResult = $0
                                            callingCompletionExpectation.fulfill()
        }

        // wait for expectation
        wait(for: [callingCompletionExpectation], timeout: 0.1)
        
        // assert

        //check correct method called
        XCTAssertNotNil(receivedResult)

        //check correctness of data
        switch receivedResult {
        case .failure(let error):
            guard let error = error as? MockError else {
                XCTFail("data not delivered as expected")
                return
            }
            XCTAssertEqual(error, MockError.mockError)
        default:
            XCTFail("data not delivered correctly")
        }
    }
    
    func testRetrieveSucceedWithInvalidJson() {
        let sampleJson = "{"
        mockServiceLauncher.mockResult = Result<Data, Error>.success(Data(sampleJson.utf8))
        
        // act
        var receivedResult: Result<Earthquakes, Error>?
        let callingCompletionExpectation = expectation(description: "callingCompletion")
        interactor.retrieveDataFromServer(north: mockNorth,
                                          south: mockSouth,
                                          east: mockEast,
                                          west: mockWest) {
                                            receivedResult = $0
                                            callingCompletionExpectation.fulfill()
        }

        // wait for expectation
        wait(for: [callingCompletionExpectation], timeout: 0.1)
        
        // assert

        //check correct method called
        XCTAssertNotNil(receivedResult)

        //check correctness of data
        switch receivedResult {
        case .failure(let error):
            guard let error = error as? InteractorError else {
                XCTFail("data not delivered as expected")
                return
            }
            XCTAssertEqual(error, InteractorError.parsingError)
        default:
            XCTFail("data not delivered correctly")
        }
    }
}
