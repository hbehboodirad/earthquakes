//
//  Copyright © 2020 HBR. All rights reserved.
//

import XCTest
@testable import BehboodiEarthquake

class InteractortErrorTests: XCTestCase {
    
    func testLocalizationMessages() {
        XCTAssertEqual(InteractorError.parsingError.localizedDescription, localized("webService.errors.dataProcess"))
    }
}
