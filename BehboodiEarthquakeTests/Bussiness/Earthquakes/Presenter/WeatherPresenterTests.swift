//
//  Copyright © 2020 HBR. All rights reserved.
//

import XCTest
@testable import BehboodiEarthquake

class WeatherPresenterTests: XCTestCase {

    var presenter: EarthquakesPresenter!
    var mockRouter: MockRouter!
    var mockInteractor: MockInteractor!

    let testEarthquake = Earthquake(dateTime: "2020-01-01 01:01:01", longitude: 1.2, latitude: 3.4, magnitude: 5)
    
    override func setUp() {
        presenter = EarthquakesPresenter()
        mockRouter = MockRouter()
        mockInteractor = MockInteractor()

        presenter.interactor = mockInteractor
        presenter.router = mockRouter
    }
    
    func testLoadData() {
        // arrange
        var loadingStartedCalled = false
        let onLoadingStarted: () -> Void = {
            loadingStartedCalled = true
        }
        let output = presenter.bind(onLoadingStarted: onLoadingStarted, onDataLoaded: { _ in }, onLoadingFailed: { _ in})
        
        // act
        output.onDataRequired()

        // assert ...

        //1- called interactor?
        XCTAssertTrue(mockInteractor.retrieveMethodCalled)

        //2- called loading started?
        XCTAssertTrue(loadingStartedCalled)
        
        //3- passed correct data
        // In a real app following values comes from user interactions
        XCTAssertEqual(mockInteractor.receivedNorth, 44.1)
        XCTAssertEqual(mockInteractor.receivedSouth, -9.9)
        XCTAssertEqual(mockInteractor.receivedEast, -22.4)
        XCTAssertEqual(mockInteractor.receivedWest, 55.2)
    }

    func testRetrieveDataSuccessfully() {
        // arrange
        let testEarthquakeWithHightMagnitude = Earthquake(dateTime: "2020-02-22 02:02:02", longitude: 1.22, latitude: 3.42, magnitude: 9)
        let wrongDateEarthquake = Earthquake(dateTime: "20200222", longitude: 1.22, latitude: 3.42, magnitude: 9)
        let earthquakes = Earthquakes(earthquakes: [testEarthquake, testEarthquakeWithHightMagnitude, wrongDateEarthquake])
        mockInteractor.mockResult = Result.success(earthquakes)
        var receivedData: [EarthquakeViewModel]?
        let onDataLoaded: ([EarthquakeViewModel]) -> Void = {
            receivedData = $0
        }
        var loadingFailedCalled = false
        let onLoadingFailed: (String) -> Void = { _ in
            loadingFailedCalled = true
        }
        let output = presenter.bind(onLoadingStarted: {}, onDataLoaded: onDataLoaded, onLoadingFailed: onLoadingFailed)
        
        // act
        output.onDataRequired()
        
        // assert ...
        
        guard let data = receivedData, data.count == 3 else {
            XCTFail("didn't receive expected data")
            return
        }
        XCTAssertEqual(data[0].date, "1 Jan 2020 01:01")
        XCTAssertEqual(data[0].longitude, "\(testEarthquake.longitude)")
        XCTAssertEqual(data[0].latitude, "\(testEarthquake.latitude)")
        XCTAssertEqual(data[0].magnitude, "\(testEarthquake.magnitude)")
        XCTAssertEqual(data[0].backgroundColor, UIColor.surface)
        XCTAssertEqual(data[0].textColor, UIColor.onSurface)
        
        XCTAssertEqual(data[1].date, "22 Feb 2020 02:02")
        XCTAssertEqual(data[1].longitude, "\(testEarthquakeWithHightMagnitude.longitude)")
        XCTAssertEqual(data[1].latitude, "\(testEarthquakeWithHightMagnitude.latitude)")
        XCTAssertEqual(data[1].magnitude, "\(testEarthquakeWithHightMagnitude.magnitude)")
        XCTAssertEqual(data[1].backgroundColor, UIColor.accent)
        XCTAssertEqual(data[1].textColor, UIColor.onAccent)

        XCTAssertEqual(data[2].date, "")
        
        XCTAssertFalse(loadingFailedCalled)
    }
    
    func testRetrieveFailed() {
        // arrange
        mockInteractor.mockResult = Result.failure(MockError.mockError)
        var dataLoadedCalled = false
        let onDataLoaded: ([EarthquakeViewModel]) -> Void = { _ in
            dataLoadedCalled = true
        }
        var receivedErrorMessage: String?
        let onLoadingFailed: (String) -> Void = {
            receivedErrorMessage = $0
        }
        let output = presenter.bind(onLoadingStarted: {}, onDataLoaded: onDataLoaded, onLoadingFailed: onLoadingFailed)
        
        // act
        output.onDataRequired()
        
        // assert ...
        
        guard let errorMessage = receivedErrorMessage else {
            XCTFail("didn't receive expected data")
            return
        }
        XCTAssertEqual(errorMessage, MockError.mockError.localizedDescription)
        
        XCTAssertFalse(dataLoadedCalled)
    }
    
    func testOnItemSelected() {
        // arrange
        let item = EarthquakeViewModel(earthquake: testEarthquake)
        let navigationController = UINavigationController()
        let output = presenter.bind(onLoadingStarted: {}, onDataLoaded: { _ in }, onLoadingFailed: { _ in})
        
        // act
        output.onItemSelected(item, navigationController)
        
        // assert ...
        
        XCTAssertTrue(mockRouter.routerCalled)
        
        guard
            let receivedItem = mockRouter.receivedEarthquake,
            let receivedNavigationController = mockRouter.receivedNavigationController
            else {
                XCTFail("didn't get data as expected")
                return
        }
        
        XCTAssertEqual(receivedItem, item.earthquake)
        XCTAssertEqual(receivedNavigationController, navigationController)
    }
}
